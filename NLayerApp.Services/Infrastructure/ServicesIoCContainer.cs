﻿using Microsoft.Extensions.DependencyInjection;
using NLayerApp.Data.Infrastructure;
using NLayerApp.Services.Common.Authors;
using NLayerApp.Services.Common.Books;
using NLayerApp.Services.Common.Categories;
using NLayerApp.Services.Common.Countries;
using NLayerApp.Services.Common.Users;

namespace NLayerApp.Services.Infrastructure
{
    public static class ServicesIoCContainer
    {
        public static IServiceCollection AddToContainer(this IServiceCollection services)
        {
            DataIoCContainer.AddToContainer(services); //Go to upper layer

            //Registrations inside this project
            services.AddScoped(typeof(IBookService), typeof(BookService));
            services.AddScoped(typeof(IAuthorService), typeof(AuthorService));
            services.AddScoped(typeof(ICategoryService), typeof(CategoryService));
            services.AddScoped(typeof(IUserService),typeof(UserService));
            services.AddScoped(typeof(ICountryService),typeof(CountryService));
            
            return services;
        }
    }
}
