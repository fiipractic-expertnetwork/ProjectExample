﻿using NLayerApp.Services.Common.Users.Dto;

namespace NLayerApp.Services.Common.Users
{
    public interface IUserService
    {
        UserDto Login(string email, string password);
        UserDto Register(string email, string password, string profilePicture);
    }
}